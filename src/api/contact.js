import { fetchApi } from '../utils/api';

export const listContact = () => fetchApi({
  method: 'GET',
});

export const getContact = url => fetchApi({
  url,
  method: 'GET',
});

export const addContact = data => fetchApi({
  method: 'POST',
  data,
});

export const deleteContact = url => fetchApi({
  url,
  method: 'DELETE',
});

export const updateContact = (url, data) => fetchApi({
  url,
  method: 'PUT',
  data,
});