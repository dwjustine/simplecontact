import React from 'react';
import { View, Text } from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import tw from '../../../tailwind';

export default function ErrorMessage({ error = {} }) {
  const { type, message } = error;

  const errorMessage = type === 'required'
    ? (message || 'This field is required')
    : message;

  return (
    <View style={tw('w-full rounded px-1.5 py-px mt-2')}>
      <View style={tw('flex-row items-center')}>
        <MaterialIcon name="error" style={tw('pt-px text-red-500')} size={12} />
        <View style={tw('flex-1')}>
          <Text style={tw('ml-1 text-red-500')}>{errorMessage}</Text>
        </View>
      </View>
    </View>
  );
}