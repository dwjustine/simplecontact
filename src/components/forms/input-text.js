import React from 'react';
import { TextInput, View } from 'react-native';
import { Controller } from 'react-hook-form';
import tw, { getColor } from '../../../tailwind';
import ErrorMessage from './error-message';

export default function InputText({
  name, control, rules, defaultValue = '', errors = {}, disabled,
  containerStyle, style, placeholder, keyboardType,
  secureTextEntry, ...props
}) {
  return (
    <Controller
      control={control}
      render={({ onChange, value }) => (
        <View style={containerStyle}>
          <TextInput
            style={[
              tw(
                'bg-white rounded-lg h-12 text-black font-semibold text-base',
                'px-4',
                'border border-gray-400',
              ),
              style,
            ]}
            placeholderTextColor={getColor('gray-400')}
            placeholder={placeholder}
            onChangeText={onChange}
            value={value}
            keyboardType={keyboardType}
            secureTextEntry={secureTextEntry}
            editable={!disabled}
            {...props}
          />
          {errors[name] && <ErrorMessage error={errors[name]} />}
        </View>
      )}
      name={name}
      rules={rules}
      defaultValue={defaultValue}
    />
  );
}