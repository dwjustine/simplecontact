import React from 'react';
import { SafeAreaView } from 'react-native';
import StatusBar from './status-bar';
import tw from '../../../tailwind';

export default function BaseLayout({ children }) {
  return (
    <>
      <StatusBar />
      <SafeAreaView style={tw('flex-1')}>
        {children}
      </SafeAreaView>
    </>
  );
}
