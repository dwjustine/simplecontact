import React from 'react';
import { StatusBar as StatusBarRN } from 'react-native';

export default function StatusBar() {
  return (
    <StatusBarRN
      translucent
      backgroundColor="transparent"
      barStyle="dark-content"
    />
  );
}
