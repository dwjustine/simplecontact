import React from 'react';
import Toast from 'react-native-toast-message';
import { RootLayout } from './components/layouts';
import Navigations from './navigations/root';


export default function App() {
  return (
    <RootLayout>
      <Navigations />
      <Toast ref={ref => Toast.setRef(ref)} />
    </RootLayout>
  );
}
