import React, { useEffect, useState } from 'react';
import { ScrollView, Text, View, TouchableOpacity } from 'react-native';
import Toast from 'react-native-toast-message';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { listContact, deleteContact } from '../api/contact';
import tw, { getColor } from '../../tailwind';


export default function List({ navigation }) {
  const [contacts, setContacts] = useState([]);
  useEffect(() => {
    listContact().then(res => setContacts(res.data));
  }, []);
  const handleDelete = (id) => {
    deleteContact(id).then((res) => {
      Toast.show({
        type: 'success',
        visibilityTime: 3000,
        text1: 'Success',
        text2: res.message,
      });
      listContact().then(res2 => setContacts(res2.data));
    }).catch(err =>
      Toast.show({
        type: 'error',
        visibilityTime: 3000,
        text1: 'Error',
        text2: err.response.data.message,
      }));
  };

  return (
    <>
      <ScrollView>
        {contacts.map(contact => (
          <TouchableOpacity
            key={contact.id}
            style={tw('border rounded-lg border-gray-400 m-3 p-3 flex-row justify-between items-center flex-grow-0 bg-white')}
            onPress={() => navigation.navigate('Form', { id: contact.id })}
          >
            <View style={tw('flex-initial')}>
              <Text style={tw('font-bold text-xl text-blue-800')}>{contact.firstName}</Text>
              <Text style={tw('font-semibold text-lg')}>{contact.lastName}</Text>
              <Text style={tw('text-sm')}>{contact.age}</Text>
            </View>
            <TouchableOpacity onPress={() => handleDelete(contact.id)}>
              <MaterialIcon name="delete" size={36} color={getColor('gray-400')} style={tw('flex-initial')} />
            </TouchableOpacity>
          </TouchableOpacity>
        ))}
      </ScrollView>
      <TouchableOpacity style={tw('absolute bottom-0 right-0 bg-blue-500 rounded-full p-4 m-5')} onPress={() => navigation.navigate('Form')}>
        <MaterialIcon name="add" size={24} color={getColor('white')} />
      </TouchableOpacity>
    </>
  );
}