import React, { useEffect } from 'react';
import { Text, TouchableOpacity } from 'react-native';
import { useForm } from 'react-hook-form';
import Toast from 'react-native-toast-message';
import { getContact, updateContact, addContact } from '../api/contact';
import tw from '../../tailwind';
import InputText from '../components/forms/input-text';

export default function Form({ route, navigation }) {
  const routeParams = route.params || {};
  const { id } = routeParams;
  const { control, errors, handleSubmit, watch, setValue } = useForm();

  useEffect(() => {
    if (id) {
      getContact(id).then((res) => {
        setValue('firstName', res.data.firstName);
        setValue('lastName', res.data.lastName);
        setValue('age', `${res.data.age}`);
      });
    }
  }, []);

  const onSubmitForm = (formVal) => {
    if (id) {
      updateContact(id, formVal).then((res) => {
        Toast.show({
          type: 'success',
          visibilityTime: 3000,
          text1: 'Success',
          text2: res.message,
        });
        navigation.reset({
          index: 0,
          routes: [{ name: 'List' }],
        });
      }).catch(err =>
        Toast.show({
          type: 'error',
          visibilityTime: 3000,
          text1: 'Error',
          text2: err.response.data.message,
        }));
    } else {
      addContact(formVal).then((res) => {
        Toast.show({
          type: 'success',
          visibilityTime: 3000,
          text1: 'Success',
          text2: res.message,
        });
        navigation.reset({
          index: 0,
          routes: [{ name: 'List' }],
        });
      }).catch(err =>
        Toast.show({
          type: 'error',
          visibilityTime: 3000,
          text1: 'Error',
          text2: err.response.data.message,
        }));
    }
  };

  return (
    <>
      <InputText
        control={control}
        name="firstName"
        placeholder="First Name"
        errors={errors}
        rules={{ required: true }}
        containerStyle={tw('m-4')}
      // defaultValue={contact.firstName}
      />
      <InputText
        control={control}
        name="lastName"
        placeholder="Last Name"
        errors={errors}
        rules={{ required: true }}
        containerStyle={tw('m-4')}
      />
      <InputText
        control={control}
        name="age"
        placeholder="Age"
        errors={errors}
        rules={{ required: true }}
        keyboardType="numeric"
        containerStyle={tw('m-4')}
      />
      <TouchableOpacity
        disabled={!watch('firstName') && !watch('lastName') && !watch('age')}
        onPress={handleSubmit(onSubmitForm)}
        style={[tw('m-4 items-center bg-blue-600 p-2 rounded-lg', !watch('firstName') && !watch('lastName') && !watch('age') && 'bg-gray-500')]}
      >
        <Text style={tw('font-semibold text-lg text-white')}>Submit</Text>
      </TouchableOpacity>
    </>
  );
}