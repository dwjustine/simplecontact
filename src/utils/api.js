import axios from 'axios';
import { BASE_URL, API_TIMEOUT } from './app-properties';

export const fetchApi = async ({
  url = '', data, params, method = 'GET', headers, ...rest
}) => {
  console.info(
    `%c[${method}]%c ${url || `${BASE_URL}${url}`}`,
    `background: green; color: white; padding: 0 2px;`,
    `color: green;`,
    { params, data },
  );

  const response = await axios({
    timeout: API_TIMEOUT,
    baseURL: BASE_URL,
    url,
    method,
    data,
    params,
    headers,
    ...rest,
  });

  console.log(
    `%c[${response.status}]%c%c[${method}]%c ${url || `${BASE_URL}${url}`}`,
    'background: green; color: white; padding: 0 2px;',
    '',
    'background: black; color: white; padding: 0 2px;',
    'color: green;',
    response.data,
  );

  return response.data;
};

export default fetchApi;