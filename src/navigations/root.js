import 'react-native-gesture-handler';
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import List from './../screens/list';
import Form from './../screens/form';

const Stack = createStackNavigator();

export default function Navigations() {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="List"
      >
        <Stack.Screen
          name="List"
          component={List}
        />
        <Stack.Screen
          name="Form"
          component={Form}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
